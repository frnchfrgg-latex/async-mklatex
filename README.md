async-mklatex
=============

This is a very small python-based ViM plugin to enable background compilation
of documents by LaTeXMk in (G)Vim.

## Settings

The plugin redefines `<LocalLeader>ll` locally to the buffer to map to its
asynchronous compilation method which is the function `TexAsyncCompile()`.
If you want to prevent that you can set:
* `no_plugin_maps` (will probably disable all plugin mappings)
* `no_tex_maps` (will probably disable all mappings of TeX ftplugins)
* `no_asynccompile_maps` (specific to this plugin)

## Dependencies

For this plugin to work, the following must be satisfied:

* ViM must have been compiled with Python (2 or 3) support.
* ViM must have been compiled with the `+timers` feature.
* You must currently have the ViM-LaTeXSuite plugin installed, but I intend
to change that

## Installing with Vundle

Add the line
```
Plugin 'https://gitlab.com/frnchfrgg-latex/async-mklatex.git'
```
to your `.vimrc` (or similar), resource it (or restart ViM) and run `:VundleUpdate`.

## Installing manually

Just copy the `after/ftplugin/tex_asyncmk.vim` file to
`~/.vim/after/ftplugin/tex_asyncmk.vim`.

The `after` directory is needed to ensure other plugins like LatexSuite do not
overwrite the `\ll` shortcut.
