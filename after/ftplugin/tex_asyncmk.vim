" Vim plugin to compile TeX files in the background with latexmk
" ---------------
"  Needs: LaTeXMk; a Vim compiled with timers and python 2 or 3 support.
"
" Copyright (C) 2018  Julien "_FrnchFrgg_" RIVAUD
"
" This program is free software: you can redistribute it and/or modify
" it under the terms of the GNU General Public License as published by
" the Free Software Foundation, either version 3 of the License, or
" (at your option) any later version.
"
" This program is distributed in the hope that it will be useful,
" but WITHOUT ANY WARRANTY; without even the implied warranty of
" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
" GNU General Public License for more details.
"
" You should have received a copy of the GNU General Public License
" along with this program.  If not, see <http://www.gnu.org/licenses/>.

if !has("timers") || !has("pythonx")
    finish
endif

if !exists("no_plugin_maps") && !exists("no_tex_maps") && !exists("no_asynccompile_maps")
    nmap <buffer> <LocalLeader>ll :call TexAsyncCompile()<CR>
    vmap <buffer> <LocalLeader>ll :call TexAsyncCompile()<CR>
endif

if exists("g:loaded_asyncCompile")
    finish
endif
let g:loaded_asyncCompile = 1

pythonx << END
import vim
from gi.repository import GLib
import subprocess

current_compile = None
END

function AsyncTeX_check_compile(timer)
pythonx << END
try:
    mkl, mainFile = current_compile
except TypeError:
    shouldrun = False
    vim.command("call timer_stop({})".format(vim.eval("a:timer")))
else:
    shouldrun = mkl.poll() is not None
if shouldrun:
    w = vim.current.window
    l, c = w.cursor
    vim.command("TCLevel {}".format(
            vim.eval("Tex_GetVarValue('Tex_IgnoreLevel')")))
    vim.command("silent! cfile {}.log".format(mainFile))
    try:
        vim.command("call Tex_SetupErrorWindow()")
    except vim.error:
        pass
    current_compile = None
    vim.command("call timer_stop({})".format(vim.eval("a:timer")))
    vim.current.window = w
    nl, _ = w.cursor
    if nl == l: w.cursor = l, c
    vim.command("redraw!")
    vim.command("echomsg 'Ran mklatex'")
END
endfunction

function TexAsyncCompile()
pclose
cclose
pythonx << END
if current_compile is None:
    mklatex = subprocess.Popen(
        ["/usr/bin/env", "latexmk", "-lualatex", "-cd", "-recorder",
         "-pdflatex=lualatex -synctex=1 -interaction=nonstopmode %O %S",
         "-pdf", "-dvi-", "-ps-",
         vim.eval("Tex_GetMainFileName(':p')")]
         ,stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
        )
    current_compile = mklatex, vim.eval("Tex_GetMainFileName(':p:r')")
    vim.command(
        "call timer_start(2000, 'AsyncTeX_check_compile', {'repeat': -1})"
    )
END
endfunction
